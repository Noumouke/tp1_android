package apk.tp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    TextView t1;
    TextView v1;
    TextView v2;
    TextView v3;
    TextView v4;
    TextView v5;
   // Button save;

    Animal animal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        String name = getIntent().getStringExtra("name");
        animal = AnimalList.getAnimal(name);

        ImageView image = findViewById(R.id.im);
        int id = getResources().getIdentifier(animal.getImgFile(),"drawable",getPackageName());
        image.setImageResource(id);

        t1 = (TextView) findViewById(R.id.t1);
        t1.setText(name);

        v1 = findViewById(R.id.v1);
        v1.setText(animal.getStrHightestLifespan());

        v2 = findViewById(R.id.v2);
        v2.setText(animal.getStrGestationPeriod());

        v3 = findViewById(R.id.v3);
        v3.setText(animal.getStrBirthWeight());

        v4 = findViewById(R.id.v4);
        v4.setText(animal.getStrAdultWeight());

        v5= findViewById(R.id.v5);
        v5.setText(animal.getConservationStatus());

          Button save = findViewById(R.id.button);
          save.setOnClickListener(new View.OnClickListener() {
            @Override
           public void onClick(View v) {
                animal.setConservationStatus(v5.getText().toString());
                Toast.makeText(getApplication(),"Sauvegarde effectuée",Toast.LENGTH_LONG).show();
            }


        });




            }
        }



